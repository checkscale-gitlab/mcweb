
 ------------------------------
 Xpress Wordpress Install
 ------------------------------

 Copyright (c) 2019 - Monaco F. J. <monaco@usp.br>
 This is free open source software under the terms of GNU GPL vr.3.
 available at https://www.gnu.org/licenses/gpl-3.0.txt

 ABOUT
 ------------------------------

 Xpress Wordpress Install (XpressWPi) is meant as a handy code template
 to quickly deploying a containerized WordPress [1] installation.

 The latest public release of XpressWPi, brought by XpressSource Toolbox
 project [2], is available at its official repository [3].
 
 For authoring and licensing information please refer to the companion files
 AUTHOR and LICENSING, respectively.

 QUICK SETUP
 ------------------------------

 1. Download the project from the repository [3].
 2. From the top project's source directory, execute

    	 $ make

    And follow the instructions.

 3. Even without configuring anything, the command

    	 $ make up

    should build and start the application. Point your web browswer to
    localhost, por 8080, and you should access a pristine WordPress
    installation ready to be used.

 4. You may (and really should) edit the configuration files

     	src/cms.conf
     	src/db.conf

     to further complete the local set up.


  APPLICATION BASICS
  ------------------------------
 
  XpressWPi is a multi-container application using Docker technology.
  
  By default WordPress CMS (content management system) runs in one
  container, while the database backend, by default based on MariaDB 
  (the open source fork of the known MySQL software), runs in another
  container.  A working knowledge of Wordpress, Docker and Docker Compose,
  may be helpful to further develop the application from Xpress WPi
  template code.

  XpressXPi uses permanent storage by means of Docker volumes. Their
  file system mount points are specified at .env file (at the top
  source directory).


  IMPORTANT NOTES
  ------------------------------

  XpressWPi is in early development stages.
  If you'd be so kind as to help by reporting bugs or contributing code,
  please, get in contact to authors.


  CONFIGURATION AND CUSTOMIZATION
  ------------------------------

  CMS and DB configuration parameters are initially copied from templates
  at lib/*.conf files to src/cms.conf and src/db.conf. These files contain
  the backend set up arguments, including the database name and access
  credentials. You are strongly recommended to edit these files.

  Name and version of the Docker images for the CMS and DB are initialized
  from default values at lib/*.env and are copied to top source directory's
  .env file. You may safely stick with the defaults unless you want something
  different.

  DEVELOPMENT AND DEPLOYMENT
  ------------------------------

  You may build the application, execute and test locally.
  You may then deploy the application by copying the project source to
  the production server and building the application there. Permanent
  storage is secured by volumes which you may back up and restore by
  using the usual docker functionality.

  REFERENCES
  ------------------------------

  [1] WordPress: https://wordpress.com/
  [2] XpressSource Toolbox: https://gitlab.com/xpressource
  [3] XpressWPi: https://gitlab.com/xpressource/xpresswpi

 