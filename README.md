
# CCOS's web home

This is the source code used to build the Open Source Competence 
Center (CCOS) <sup>1</sup> web site.

<hr />

<small>
(1) CCOS - Centro de Competência em Open Source, ICMC, University of São Paulo:
<a href="www.ccos.icmc.usp.br">www.ccos.icmc.usp.br</a>
</small>